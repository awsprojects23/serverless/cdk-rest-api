from app import create_app

app = create_app()


# Lambda handler function
def lambda_handler(event, context):
    # Use Flask's request context to handle the Lambda event
    with app.test_request_context(
        path=event["path"],
        method=event["httpMethod"],
        headers=event["headers"],
        query_string=event["queryStringParameters"],
    ):
        try:
            response = app.full_dispatch_request()

            # Format the response for Lambda/API Gateway
            return {
                "statusCode": response.status_code,
                "headers": dict(response.headers),
                "body": response.get_data().decode("utf-8"),
            }
        except Exception as e:
            return {"statusCode": 500, "body": "Internal Server Error from app"}
