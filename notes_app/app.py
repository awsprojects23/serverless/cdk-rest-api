from flask import Flask
from flask_cors import CORS

from routes.notes import notes_api


def create_app():
    app = Flask(__name__)
    CORS(app)
    # Register your Blueprint(s)
    app.register_blueprint(notes_api)

    return app


if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)
