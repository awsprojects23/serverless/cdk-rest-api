from flask import Blueprint, request, jsonify
from services.notes_service import create_note, get_notes

notes_api = Blueprint("notes_api", __name__)

# generate comments for each route


@notes_api.route("/notes", methods=["GET"])
def get_notes_route():
    notes = get_notes()
    return jsonify(notes)


@notes_api.route("/notes/<nid>", methods=["GET"])
def get_note_by_nid_route(nid):
    notes = get_notes()
    for note in notes:
        if nid == note["noteId"]:
            return jsonify([note])
    else:
        return jsonify([{}])


@notes_api.route("/notes", methods=["POST"])
def create_note_route():
    data = request.get_json()
    if not data or "content" not in data:
        return jsonify({"error": "Invalid input"}), 400

    content = data["content"]
    message = create_note(content)
    return jsonify({"message": message})
