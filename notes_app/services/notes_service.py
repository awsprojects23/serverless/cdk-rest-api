import boto3
import os
import uuid
from botocore.exceptions import ClientError

# Use the DynamoDB table name and region from environment variables
table_name = os.environ.get("DYNAMODB_TABLE_NAME", "NotesTable")
region = os.environ.get("AWS_REGION", "eu-central-1")

dynamodb = boto3.resource("dynamodb", region_name=region)
notes_table = dynamodb.Table(table_name)  # Use the specified table name


def get_notes():
    try:
        response = notes_table.scan()
        notes = response.get("Items", [])
        return notes
    except ClientError as e:
        return [{"error": str(e)}]


def create_note(content):
    note_id = str(uuid.uuid4())
    try:
        notes_table.put_item(Item={"noteId": note_id, "content": content})
        return "Note created successfully"
    except ClientError as e:
        return str(e)
