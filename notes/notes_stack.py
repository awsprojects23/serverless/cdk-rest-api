from aws_cdk import (
    Stack,
    aws_dynamodb as dynamodb,
    aws_lambda as _lambda,
    aws_apigateway as apigateway,
    RemovalPolicy,
    aws_iam as iam,
)

from constructs import Construct


class NotesStack(Stack):
    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # Define the DynamoDB table for notes
        # Provide the table name and partition key along with the removal policy
        notes_table = dynamodb.Table(
            self,
            "NotesTable",
            partition_key=dynamodb.Attribute(
                name="noteId", type=dynamodb.AttributeType.STRING
            ),
            removal_policy=RemovalPolicy.DESTROY,  # Only for development, use other policies for production.
        )

        # Define the Lambda layer
        # Provide the path to the layer ZIP archive and the runtime
        # to create the layer, go to the layer folder and run: zip -r layer.zip .
        # the layer.zip will be created in the layer folder
        # Make sure to have python/ directory in the layer folder
        notes_layer = _lambda.LayerVersion(
            self,
            "NotesLayer",
            code=_lambda.Code.from_asset(
                "layer/layer.zip"
            ),  # Specify the path to your Lambda layer ZIP archive
            compatible_runtimes=[_lambda.Runtime.PYTHON_3_8],  # Specify the runtime
            description="Lambda layer for Notes app",  # Add a description
        )

        # Define the Lambda execution role with DynamoDB permissions
        lambda_role = iam.Role(
            self,
            "LambdaRole",
            assumed_by=iam.ServicePrincipal("lambda.amazonaws.com"),
        )

        # Attach AmazonDynamoDBFullAccess policy to the Lambda role
        lambda_role.add_managed_policy(
            iam.ManagedPolicy.from_aws_managed_policy_name("AmazonDynamoDBFullAccess")
        )

        # Define a single Lambda function that handles all routes
        all_routes_lambda = _lambda.Function(
            self,
            "AllRoutesFunction",
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler="lambda_handler.lambda_handler",  # Import the custom handler function
            code=_lambda.Code.from_asset("notes_app/"),
            environment={
                "DYNAMODB_TABLE_NAME": notes_table.table_name  # Pass the DynamoDB table name as an environment variable
            },
            role=lambda_role,
        )

        # Add the Lambda layer to the Lambda function
        all_routes_lambda.add_layers(notes_layer)

        # Define the API Gateway
        api = apigateway.RestApi(
            self,
            "NotesApi",
            rest_api_name="Notes API",
            description="API for managing notes",
        )

        # Define the catch-all proxy resource
        proxy_resource = api.root.add_resource("{proxy+}")

        # Define the Lambda integration for the catch-all proxy
        proxy_lambda_integration = apigateway.LambdaIntegration(all_routes_lambda)

        # Add ANY method to the catch-all proxy resource and integrate it with the Lambda function
        proxy_resource.add_method("ANY", proxy_lambda_integration)

        # Create a deployment and associate it with the API
        # Deployment is a "snapshot" of the API Gateway
        deployment = apigateway.Deployment(
            self,
            "Deployment",
            api=api,  # Reference to your API Gateway instance
        )

        # Deploy the API to a stage (e.g., "prod")
        api.deployment_stage = apigateway.Stage(
            self,
            "DevStage",
            stage_name="dev",
            deployment=deployment,
        )

        # Export the DynamoDB table and Lambda function as attributes of the stack
        self.notes_table = notes_table
        self.create_all_routes_lambda = all_routes_lambda
