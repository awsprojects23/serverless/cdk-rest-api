import aws_cdk as core
import aws_cdk.assertions as assertions

from notes.notes_stack import NotesStack

# example tests. To run these tests, uncomment this file along with the example
# resource in notes/notes_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = NotesStack(app, "notes")
    template = assertions.Template.from_stack(stack)

#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })
