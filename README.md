
# Hands-on with AWS CDK and Flask

**Description**: The purpose of this project is to demonstrate how to use AWS CDK to deploy a Flask application to AWS Lambda. To make it more interesting, the Flask application will be a simple REST API that will be deployed to AWS API Gateway. The REST API will be used to manage a list of notes. The notes will be stored in a DynamoDB table.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- **AWS Account:** You need an AWS account to deploy resources.
- **AWS CLI:** Install and configure the AWS Command Line Interface ([AWS CLI Installation Guide](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)).
- **Node.js and NPM:** Install Node.js and NPM ([Node.js Downloads](https://nodejs.org/en/download/)).
- **Python:** Install Python 3 ([Python Downloads](https://www.python.org/downloads/)).
- **AWS CDK:** Install AWS CDK globally (`npm install -g aws-cdk`).

## Steps to create the application

### Create a new project

Create a new project directory and initialize a new CDK project.

```bash
mkdir notes
cd notes
cdk init app --language python
```

The last command will create a new CDK project in the current directory. The project will be written in Python. The project will contain a single stack called NotesStack. The stack will be defined in the file notes_stack.py.

### Add the REST API

You can simply add the notes_app folder to the project. The notes_app folder contains the Flask application. The application is a simple REST API that will be used to manage a list of notes. The notes will be stored in a DynamoDB table.

Once added, the project structure should look like this:

```bash
.
├── README.md
├── .venv
├── .gitignore
├── app.py
├── cdk.json
├── notes_app
│   ├── __init__.py
│   ├── app.py
│   ├── requirements.txt
│   └── tests
│       ├── __init__.py
│       └── test_app.py
├── notes_stack.py
├── requirements.txt
├── requirements_dev.txt
└── source.bat
```

### Edit the NotesStack

Edit the NotesStack to add the following resources:

- **Lambda Function:** The Lambda function will be used to run the Flask application.
- **API Gateway:** The API Gateway will be used to expose the REST API.
- **IAM Role:** The IAM role will be used to grant the Lambda function permissions to access the DynamoDB table.
- **DynamoDB Table:** The DynamoDB table will be used to store the notes.
- **Lambda Layer:** The Lambda layer will be used to add the Flask application to the Lambda function.


### Install Requirements

Install the requirements for the CDK project.

```bash
cd notes
pip install -r requirements.txt
```

Install the requirements for the Flask application.

```bash
cd notes/notes_app
pip install -r requirements.txt
```

### Prepare Dependency Layer

Create a new directory called **layer**. Inside the directory create another directory **python/**. This directory will be used to store the dependencies for the Lambda function.

```bash
mkdir layer
cd layer
mkdir python
```

Install the dependencies for the Lambda function.

```bash
cd notes_app
pip install -r requirements.txt -t ../layer/python/
```
This will install the dependencies in the **layer/python/** directory in the root folder. The dependencies will be used to create a Lambda layer. We need to create a zip file containing the dependencies.

```bash
cd ../layer
zip -r dependencies.zip *
```


### Bootstrap the AWS environment

Bootstrap the AWS environment to prepare it for CDK deployments.

```bash
cdk bootstrap
```

### Deploy the application

Deploy the application to AWS.

```bash
cdk deploy
```


### 6. Test the application

Test the application by adding a new note.

```bash
curl -X POST https://<api-id>.execute-api.<region>.amazonaws.com/prod/notes -d '{"title": "My Note", "content": "This is my note."}'
```

Test the application by getting all notes.

```bash
curl https://<api-id>.execute-api.<region>.amazonaws.com/prod/notes
```

Test the application by getting a single note.

```bash
curl https://<api-id>.execute-api.<region>.amazonaws.com/prod/notes/<note-id>
```
