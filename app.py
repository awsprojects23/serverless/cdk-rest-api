#!/usr/bin/env python3
import os

import aws_cdk as cdk

from notes.notes_stack import NotesStack


app = cdk.App()
NotesStack(
    app,
    "NotesStack",
    # If you don't specify 'env', this stack will be environment-agnostic.
    # the next line specializes this stack for the AWS Account
    # and Region that are implied by the current CLI configuration.
    env=cdk.Environment(
        account=os.getenv("CDK_DEFAULT_ACCOUNT"), region=os.getenv("CDK_DEFAULT_REGION")
    ),
)

app.synth()
